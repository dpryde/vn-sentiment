import spacy
import pandas as pd
from pathlib import Path
import os
from pyvi import ViTokenizer

dir_name = os.path.dirname(os.path.realpath(__file__))
os.chdir(dir_name)

nlp_tokenize = spacy.load("vi_core_news_md")

data_path = Path("../../data")

files = ["train.xlsx", "valid.xlsx", "test.xlsx"]


def transform(t):
    token = ViTokenizer.tokenize(t)
    return token


for file in files:
    df = pd.read_excel(data_path / file, dtype=str)

    df["Content"] = df["Content"].transform(transform)

    with pd.ExcelWriter(
        data_path / (os.path.splitext(file)[0] + "_tokenized" + ".xlsx"),
        engine="xlsxwriter",
        options={"strings_to_urls": False},
    ) as writer:
        df.to_excel(writer)
