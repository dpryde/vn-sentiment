import os
import re
from typing import Callable, Pattern, List
from pathlib import Path
import ast
import pandas as pd
import swifter
import click
from tqdm import tqdm
import random
import logging
import chalk

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

data_path = Path(os.path.join(os.path.dirname(__file__), "../data"))


@click.command()
@click.option("--input-folder", required=True)
@click.option("--train", default="train")
@click.option("--valid", default="valid")
@click.option("--test", default="test")
def main(input_folder, train, valid, test):
    folder_name = input_folder
    input_folder = data_path / input_folder

    data_sets = [
        ("train", input_folder / train),
        ("valid", input_folder / valid),
        ("test", input_folder / test),
    ]

    for name, folder_path in data_sets:
        print(chalk.green(f"Processing dataset: {folder_path}"))
        comments = []
        sentiments = []
        labels = [
            name
            for name in os.listdir(folder_path)
            if os.path.isdir(folder_path / name)
        ]

        for label in labels:
            print(chalk.blue(f"Processing label: {label}"))
            commentsForLabel = []
            for file in tqdm(os.listdir(folder_path / label)):
                with open(
                    folder_path / label / file, mode="r", encoding="utf-8"
                ) as commentFile:
                    commentsForLabel.append(commentFile.read())
            comments.extend(commentsForLabel)
            sentiments.extend([label] * len(commentsForLabel))

        comments, sentiments = zip(
            *random.sample(list(zip(comments, sentiments)), len(comments))
        )

        df = pd.DataFrame({"Content": comments, "Sentiment": sentiments})

        with pd.ExcelWriter(
            data_path / (folder_name + "_" + name + ".xlsx"),
            engine="xlsxwriter",
            options={"strings_to_urls": False},
        ) as writer:
            df.to_excel(writer, index=False)



if __name__ == "__main__":
    main()

