from datetime import datetime
import os
import argparse
from pathlib import Path
import pandas as pd
from typing import Optional, Tuple
from sklearn.model_selection import train_test_split

SplitDataframes = Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]

data_path = Path(os.path.join(os.path.dirname(__file__), "../data"))


def split_data(
    df: pd.DataFrame,
    label_column: str,
    train_pct: float = 0.7,
    valid_pct: float = 0.2,
    random_state: Optional[int] = None,
) -> SplitDataframes:
    test_pct = 1 - (train_pct + valid_pct)
    X = df
    y = X[label_column]

    if random_state is None:
        random_state = int(datetime.utcnow().timestamp())

    X_train_valid, X_test = train_test_split(
        X, test_size=test_pct, random_state=random_state, stratify=y
    )

    X_train, X_valid = train_test_split(
        X_train_valid,
        train_size=train_pct / (train_pct + valid_pct),
        random_state=random_state,
        stratify=X_train_valid[label_column],
    )
    return X_train, X_valid, X_test


if __name__ == "__main__":
    cwd = os.getcwd()

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input-file",
        dest="input_file",
        help="Path of file Excel",
        required=True,
    )
    parser.add_argument(
        "-label-col",
        "--label-column",
        dest="label_column",
        help="Label column",
        required=True,
        default="Sentiment",
    )
    parser.add_argument(
        "-rnd", "--random-state", dest="random_state", help="Random state"
    )
    parser.add_argument(
        "-tpct", "--train-pct", dest="train_pct", help="Train percentage", type=float
    )
    parser.add_argument(
        "-vpct", "--valid-pct", dest="valid_pct", help="Valid percentage", type=float
    )
    parser.add_argument(
        "-ngin", "--engine", dest="engine", help="Engine of Excel", default="xlsxwriter"
    )
    parser.add_argument(
        "-pofx", "--post-fix", dest="postfix", help="Postfix to output file", default=""
    )

    args = parser.parse_args()

    df = pd.read_excel(data_path / args.input_file)
    X_train, X_valid, X_test = split_data(
        df, args.label_column, args.train_pct, args.valid_pct
    )

    output_entries = [(X_train, f"train"), (X_valid, "valid"), (X_test, "test")]
    prefix = args.input_file.split(".xlsx")[0]
    for X, output_file in output_entries:
        with pd.ExcelWriter(
            data_path
            / f"{prefix}_{output_file}{'_' if args.postfix else ''}{args.postfix}.xlsx",
            engine=args.engine,
            options={"strings_to_urls": False},
        ) as writer:
            X.to_excel(writer, index=False)

    print("Train:")
    print(X_train.head())
    print("Valid:")
    print(X_valid.head())
    print("test:")
    print(X_test.head())
