import pandas as pd
import os

def excel_to_files(input_excel_file, output_dir):
    df = pd.read_excel(input_excel_file)

    for i, row in df.iterrows():
        file_name = os.path.join(output_dir, str(i + 1) + ".txt")
        with open(file_name, "w", encoding="utf-8") as f:
            f.write(row["Content"])

if __name__ == "__main__":
    excel_to_files(
        input_excel_file = r".\foody_valid_preprocessed_notok.xlsx",
        output_dir = r".\output")
