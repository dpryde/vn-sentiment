import argparse
import os
import ast
import pandas as pd
from typing import List, Tuple, Union

ColumnMap = Tuple[str, dict]
ColumnMaps = List[ColumnMap]


def map_labels(
    df: pd.DataFrame, column_maps: Union[ColumnMap, ColumnMaps], inplace: bool = True
) -> pd.DataFrame:
    """
		This function will map column values to specified values
		column_maps: 
			- an array of tuple where 1st element is column name,
			2nd element is a dictionary with key is a column value to be mapped and
			value is a target value
	"""
    if not isinstance(column_maps, list):
        column_maps: ColumnMaps = [column_maps]

    result_df = df

    if not inplace:
        result_df = df.copy()

    for column_map in column_maps:
        column, values_map = column_map
        for value, mapped_value in values_map.items():
            df.loc[df[column] == value, column] = mapped_value
    return df


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i",
        "--input-path",
        dest="input_path",
        help="Path to Excel file",
        required=True,
    )
    parser.add_argument(
        "-o",
        "--output-path",
        dest="output_path",
        help="Output path name",
        required=True,
    )
    parser.add_argument(
        "-cols",
        "--column-maps",
        dest="column_maps",
        help="Column maps",
        required=True,
        type=ast.literal_eval,
    )
    parser.add_argument(
        "-ngin", "--engine", dest="engine", help="Engine of Excel", default="xlsxwriter"
    )
    parser.add_argument(
        "-na",
        "--na-cols",
        dest="na_cols",
        help="Replace n/a value with value with value",
        type=ast.literal_eval,
    )

    args = parser.parse_args()
    cwd = os.getcwd()

    if not os.path.isabs(args.input_path):
        args.input_path = os.path.abspath(os.path.join(cwd, args.input_path))
    if not os.path.isabs(args.output_path):
        args.output_path = os.path.abspath(os.path.join(cwd, args.output_path))

    df = pd.read_excel(args.input_path)
    result_df = map_labels(df, args.column_maps)

    if args.na_cols is not None:
        for src, target in args.na_cols.items():
            null_masks = result_df[src].isnull()
            result_df.loc[null_masks, src] = result_df.loc[null_masks, target]

    with pd.ExcelWriter(
        args.output_path, engine=args.engine, options={"strings_to_urls": False}
    ) as writer:
        result_df.to_excel(writer, index=False)
