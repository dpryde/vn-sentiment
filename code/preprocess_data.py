import os
from fastai import *
from fastai.text import *
import re
from typing import Callable, Pattern, List
import emoji
from pathlib import Path
import argparse
import ast
import pandas as pd
import swifter
from pyvi import ViTokenizer

from lib.preprocessor.utils import (
    read_related_emoji,
    read_shortcut_emoji,
    read_stem_variants_emoji,
)
from lib.preprocessor.processor import (
    remove_unrelated_emoji,
    replace_shortcut_with_emoji,
    replace_by_regexp,
    fix_improper_sentence_ending,
    lowercase,
    remove_meaningless_char,
    reduce_emoji_variants,
    remove_bullets,
    remove_number,
)


Processor = Callable[[str], str]

data_path = Path(os.path.join(os.path.dirname(__file__), "../data"))

RELATED_UNICODE_EMOJI, RELATED_EMOJI_UNICODE = read_related_emoji(
    data_path / "emoji.txt"
)
SHORTCUT_EMOJI = read_shortcut_emoji(data_path / "emoji_shortcut.txt")
SHORTCUT_REGEXP = (
    "("
    + "|".join([re.escape(shortcut) for shortcut, _ in SHORTCUT_EMOJI.items()])
    + ")"
)

STEM_VARIANTS_EMOJI = read_stem_variants_emoji(data_path / "emoji_stem_variants.txt")

MIN_LENGTH = 10

preprocess_pipeline = [
    # Replace shortcut typing with emoji unicode
    replace_shortcut_with_emoji(SHORTCUT_EMOJI, SHORTCUT_REGEXP),
    # Reduce emoji variants to one emoji
    *[
        reduce_emoji_variants(stem, variants)
        for stem, variants in STEM_VARIANTS_EMOJI.items()
    ],
    # Remove unrelated emojis
    remove_unrelated_emoji(RELATED_EMOJI_UNICODE),
    # Remove "_"
    replace_by_regexp(r"_", r" "),
    # Remove repetition of non-alphanumeric in a token
    replace_by_regexp(r"(\W)\1{1,}", r"\1"),
    # Replace by meaningless character
    remove_meaningless_char,
    # Remove bullets
    remove_bullets,
    fix_improper_sentence_ending,
    remove_number,
    lowercase,
]


def run_preprocess(t: str, pipeline: List[Processor], tokenize: bool) -> str:
    for processor in pipeline:
        t = processor(t)
    if tokenize:
        t = ViTokenizer.tokenize(t)
    return t


if __name__ == "__main__":
    #     result = run_preprocess(
    #         """*** TUYỂN Gấp Công Nhân kho sữa Vinamilk, !!!
    # - Do nhu cầu vận chuyển và phân phối sản phẩm, nhà phân phối sữa Vinamikl chúng tôi cần thêm Nam sắp xếp thùng sữa Vinamikl tại Kho (thùng 25kg). Làm việc có băng chuyền băng tải và xe nâng hỗ trợ.
    # - Làm việc vào giờ hành chính: Sáng từ 7h- 11h ; Chiều Từ 13h- 17h; Chủ nhật, ngày lễ được nghỉ.
    # - Lương: 370k/ ngày + 500(chuyên cần) + Bao ở. Lãnh lương hàng tuần .
    # - Làm việc tại TP HCM ( Q2,Q7, Thủ Đức) . Bình Dương , Đồng Nai,Long anTôi không hiểu sao
    # *** Chi tiết liên hệ:: Anh Hoàng. Tel: 0965.327.863 (Quản lí)_ ko_SMS - 🐷🐷🐷
    # 🍓🍓🍓📞📞📞• KHÔNG PHA TRỘN
    # • KHÔNG CHẤT BẢO QUẢN
    # • KHÔNG TẠP CHẤT
    # • KHÔNG CHẤT TẠO MÙI
    # • KHÔNG THU PHÍ""",
    #         preprocess_pipeline,
    #     )
    #     print(result)
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-i", "--input-file", dest="input_file", help="Excel file name", required=True
    )
    parser.add_argument(
        "-o", "--output-file", dest="output_file", help="output file", required=True
    )
    parser.add_argument(
        "-cols",
        "--columns",
        dest="columns",
        help="columns to preprocess",
        required=True,
        type=ast.literal_eval,
    )

    parser.add_argument(
        "-tok",
        action="store_true",
        dest="tokenize",
        help="Indicates whether to tokenize data",
        default=False,
    )

    args = parser.parse_args()

    print(args.tokenize)

    df = pd.read_excel(data_path / args.input_file, dtype=str)

    for column in args.columns:
        # Remove rows with content length < MIN_LENGTH
        df[column] = df[column].swifter.apply(
            lambda text: run_preprocess(text, preprocess_pipeline, args.tokenize)
        )
        df = df[df[column].map(len) >= MIN_LENGTH]

    with pd.ExcelWriter(
        data_path / args.output_file,
        engine="xlsxwriter",
        options={"strings_to_urls": False},
    ) as writer:
        df.to_excel(writer, index=False)
    print(df.head())
