import torch
import torch.nn as nn
import torch.nn.functional as F
from traitlets.utils.bunch import Bunch


class MlpAttention(nn.Module):
    """
    Multiplicative Attention Layer
    """
    def __init__(self, hidden_size):
        self.config = Bunch(hidden_size)
        self.config.hidden_size = hidden_size

        # Initialize W matrix and bias b
        self.W = torch.empty(hidden_size, hidden_size)
        nn.init.kaiming_uniform_(self.W)
        self.b = torch.empty(1, hidden_size)
        nn.init.kaiming_uniform_(self.b)

        # Initialize context vector
        self.u = torch.empty(hidden_size, 1)
        nn.init.kaiming_uniform_(self.u)

    def forward(self, inputs):
        batch_size, max_len = inputs.shape
        inputs = inputs.view(-1, self.config.hidden_size)
        tmp = torch.tanh(torch.mm(inputs, self.W) + self.b)
        tmp = torch.mm(tmp, self.u).view(batch_size, 1, max_len)
        alpha = F.softmax(tmp, dim=2)
        # size: (batch_size, 1, max_len)
        return alpha

