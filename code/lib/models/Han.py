import torch
import torch.nn as nn
import torch.nn.functional as F
from .Attention import MlpAttention
from fastai.basics import *
from fastai.text.data import *
Learner
ItemLists
TextDataBunch
TokenizeProcessor


class Han(nn.Module):
    def __init__(self, config):
        super(Han, self).__init__()
        self._init_config(config)

        # Embedding layer
        self.embedding = nn.Embedding(
            len(self.vocab.stoi), self.config.embedding_size, padding_idx=1
        )
        nn.init.kaiming_uniform_(self.embedding.weight)

        # LSTM Layer
        self.lstm_sen = nn.LSTM(
            self.config.embedding_size,
            self.config.hidden_size_sen,
            batch_first=True,
            dropout=0.5,
        )
        self.lstm_doc = nn.LSTM(
            self.config.hidden_size_sen,
            self.config.hidden_size_doc,
            batch_first=True,
            dropout=0.5,
        )

        self.fc = nn.Linear(self.config.hidden_size, 3)

        # Attention Layer
        self.attention_sen = MlpAttention(self.config.hidden_size_sen)
        self.attention_doc = MlpAttention(self.config.hidden_size_doc)

    def _init_config(self, config):
        self.config = config
        self.vocab = self.config.vocab

    def forward(self, inputs):
        bs, max_doc_length, max_sen_length = x.size()

        # Embedding tensor with size (bs, max_doc_length, max_sen_length, embedding_size)
        inputs = self.embedding(inputs)

        # This gives tensor with size: (bs*max_doc_length, max_sen_length, embedding_size )
        inputs = inputs.view(-1, max_sen_length, self.config.embedding_size)

        # LSTM for each sentence
        # Tensor size: (bs*max_doc_length, max_sen_length, hidden_size)
        hiddens_sen, _ = self.lstm_sen(inputs)
        # Attention for each sentence
        alphas_sen = self.attention_sen(hiddens_sen)
        outputs_sen = torch.squeeze(torch.bmm(alphas_sen, hiddens_sen))
        # size: (batch_size, max_doc_length, hidden_size_len)
        outputs_sen = outputs_sen.view(-1, max_doc_length, self.config.hidden_size_len)

        # LSTM for each doc
        # size: (batch_size, max_doc_length, hidden_size_doc)
        hiddens_doc, _ = self.lstm_doc(outputs_sen)
        # Attention for each doc
        alphas_doc = self.attention_doc(hiddens_doc)
        # size: (batch_size, hidden_size_doc)
        outputs_doc = torch.squeeze(torch.bmm(alphas_doc, hiddens_doc))

        scores = self.fc(outputs_doc)
        prob_scores = F.log_softmax(scores, dim=2)
        return prob_scores

