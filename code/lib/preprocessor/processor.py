import os
from typing import AnyStr
import re
import emoji
from typing import Callable
from .utils import read_related_emoji, read_shortcut_emoji


def replace_shortcut_with_emoji(
    SHORTCUT_EMOJI: dict, SHORTCUT_REGEXP: AnyStr
) -> Callable[[str], str]:
    def processor(text: str) -> str:
        pattern = re.compile(SHORTCUT_REGEXP)

        def replace_func(matchobj):
            maybe_shortcut = matchobj.group(0)
            if maybe_shortcut in SHORTCUT_EMOJI:
                return emoji.EMOJI_ALIAS_UNICODE[SHORTCUT_EMOJI[maybe_shortcut]]
            return maybe_shortcut

        text = pattern.sub(replace_func, text)
        return text

    return processor


def remove_unrelated_emoji(RELATED_UNICODE_EMOJI: dict) -> Callable[[str], str]:
    def processor(text: str) -> str:
        pattern = emoji.get_emoji_regexp()

        def replace_func(matchobj):
            maybe_unicode_emoji = matchobj.group(0)
            if (
                maybe_unicode_emoji in emoji.UNICODE_EMOJI_ALIAS
                and maybe_unicode_emoji not in RELATED_UNICODE_EMOJI
            ):
                return ""
            return maybe_unicode_emoji

        text = pattern.sub(replace_func, text)

        pattern_2 = re.compile(r"[\u2600-\u26ff\u2700-\u27bf]", re.UNICODE)
        text = pattern_2.sub("", text)
        return text

    return processor


def replace_by_regexp(pattern: AnyStr, repl: AnyStr) -> Callable[[str], str]:
    def replace(t: str):
        regexp = re.compile(pattern)
        t = regexp.sub(repl, t)
        return t

    return replace


def lowercase(t: str) -> str:
    return t.lower()


def fix_improper_sentence_ending(text: str) -> str:
    """
    Example: 'mot haiÁn ba' -> 'mot hai. Án ba'
    """
    upper_chars = "AĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴAĂÂÁẮẤÀẰẦẢẲẨÃẴẪẠẶẬĐEÊÉẾÈỀẺỂẼỄẸỆIÍÌỈĨỊOÔƠÓỐỚÒỒỜỎỔỞÕỖỠỌỘỢUƯÚỨÙỪỦỬŨỮỤỰYÝỲỶỸỴA-Z"
    lower_chars = "aăâáắấàằầảẳẩãẵẫạặậđeêéếèềẻểẽễẹệiíìỉĩịoôơóốớòồờỏổởõỗỡọộợuưúứùừủửũữụựyýỳỷỹỵaăâáắấàằầảẳẩãẵẫạặậđeêéếèềẻểẽễẹệiíìỉĩịoôơóốớòồờỏổởõỗỡọộợuưúứùừủửũữụựyýỳỷỹỵaăâáắấàằầảẳẩãẵẫạặậđeêéếèềẻểẽễẹệiíìỉĩịoôơóốớòồờỏổởõỗỡọộợuưúứùừủửũữụựyýỳỷỹỵaăâáắấàằầảẳẩãẵẫạặậđeêéếèềẻểẽễẹệiíìỉĩịoôơóốớòồờỏổởõỗỡọộợuưúứùừủửũữụựyýỳỷỹỵaăâáắấàằầảẳẩãẵẫạặậđeêéếèềẻểẽễẹệiíìỉĩịoôơóốớòồờỏổởõỗỡọộợuưúứùừủửũữụựyýỳỷỹỵaăâáắấàằầảẳẩãẵẫạặậđeêéếèềẻểẽễẹệiíìỉĩịoôơóốớòồờỏổởõỗỡọộợuưúứùừủửũữụựyýỳỷỹỵa-z"
    p = re.compile(
        "(["
        + lower_chars
        + "]+)"
        + "(["
        + upper_chars
        + "]"
        + "["
        + lower_chars
        + "]*)"
    )
    return p.sub(r"\1. \2", text)


def remove_meaningless_char(text: str) -> str:
    """
    Example: '  +   một hai\n-ba bốn' -> 'một hai\nba bốn'
    """
    p2 = re.compile(r"^\s*[-*+·]\s*(\w+)|(\w+)\s*[-*+·]\s*$", re.MULTILINE)
    return p2.sub(r"\1\2", text)


def reduce_emoji_variants(stem: str, variants: dict) -> Callable[[str], str]:
    def processor(text: str) -> str:
        pattern = emoji.get_emoji_regexp()

        def replace_func(matchobj):
            emoji_unicode = matchobj.group(0)
            emoji_alias = emoji.demojize(emoji_unicode)
            if emoji_alias in variants:
                return emoji.EMOJI_ALIAS_UNICODE[stem]
            return emoji_unicode

        text = pattern.sub(replace_func, text)
        return text

    return processor


def remove_bullets(text: str) -> str:
    pattern = re.compile(r"\d\.\s+|[a-z]\)\s+|•\s*|[A-Z]\.\s+|[IVX]+\.\s+")
    text = pattern.sub(" ", text)
    return text


def remove_number(text: str) -> str:
    pattern = re.compile(r"\S*\d\S*")
    text = pattern.sub("", text)
    return text


def remove_space_emoji_shortcut(text: str) -> str:
    pattern = re.compile(r"( \S){2,}")

    def replace_func(matchobj):
        text = matchobj.group(0)
        text = text.split(" ")
        text.insert(-1, ' ')
        text = "".join(text)
        text = " " + text
        return text

    text = pattern.sub(replace_func, text)
    return text
