import os
import re
import emoji

# Read predefined related emoji from file
def read_related_emoji(file):
    RELATED_EMOJI_UNICODE = {}
    with open(file, "r") as f:
        for line in f.readlines():
            line = line.strip()
            RELATED_EMOJI_UNICODE[emoji.EMOJI_ALIAS_UNICODE[line]] = True

    RELATED_UNICODE_EMOJI = {v: k for k, v in RELATED_EMOJI_UNICODE.items()}
    return RELATED_UNICODE_EMOJI, RELATED_EMOJI_UNICODE


# Read predefined shortcut of emoji
def read_shortcut_emoji(file):
    SHORTCUT_EMOJI = {}
    with open(file, "r") as f:
        for line in f.readlines():
            line = line.strip()
            shortcut, emoji_str = line.split(" ")
            shortcut = shortcut.strip()
            emoji_str = emoji_str.strip()
            SHORTCUT_EMOJI[shortcut] = emoji_str
    return SHORTCUT_EMOJI


# Read predefined stem and variants emoji
def read_stem_variants_emoji(file):
    STEM_AND_VARIANTS = {}
    with open(file, "r") as f:
        for line in f.readlines():
            line = line.strip()
            stem, variant = line.split(" ")
            stem = stem.strip()
            variant = variant.strip()
            if stem not in STEM_AND_VARIANTS:
                STEM_AND_VARIANTS[stem] = {}
            STEM_AND_VARIANTS[stem][variant] = True
    return STEM_AND_VARIANTS
