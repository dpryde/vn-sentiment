import click
import numpy as np
import pandas as pd
import re
from underthesea import sent_tokenize
import statistics
from tqdm import tqdm
from pathlib import Path
import os
import chalk
from pathos import multiprocessing as mp

data_path = Path(os.path.join(os.path.dirname(__file__), "../data"))


@click.command()
@click.option("--input", required=True)
@click.option("--column", required=True)
@click.option("--notok", default=False, is_flag=True)
@click.option("--count-all", default=True, is_flag=True)
@click.option("--output")
@click.option("--chunk-size", default=250, type=int)
def main(input, column, notok, count_all, output, chunk_size):
    pool = mp.Pool(mp.cpu_count())

    print(chalk.green("--- Reading file ---"))
    df = pd.read_excel(data_path / input)
    docs = df[column].values

    if notok:
        print(chalk.green("--- Untokenizing document ---"))
        docs = list(
            tqdm(
                pool.imap(lambda doc: re.sub(r"_", " ", doc), docs, chunk_size),
                total=len(docs),
            )
        )

    print(chalk.green("--- Counting #words/doc ---"))
    num_words_per_doc = list(
        tqdm(
            pool.imap(lambda doc: number_of_words(doc, count_all), docs, chunk_size),
            total=len(docs),
        )
    )

    print(chalk.green("--- Counting #sens/doc ---"))
    sens_doc = list(
        tqdm(
            pool.imap(lambda doc: sent_tokenize(doc), docs, chunk_size), total=len(docs)
        )
    )
    num_sens_per_doc = list(
        tqdm(
            pool.imap(lambda sens: len(sens), sens_doc, chunk_size), total=len(sens_doc)
        )
    )

    print(chalk.green("--- Counting #words/sen"))
    num_words_per_sen = []
    for idx, doc in tqdm(enumerate(docs)):
        sens = sens_doc[idx]
        num_words_per_sen.extend([number_of_words(sen, count_all) for sen in sens])

    print(chalk.green("--- Stats processing: #words/doc ---"))
    num_words_per_doc_statistics = {
        "mean": statistics.mean(num_words_per_doc),
        "std": statistics.stdev(num_words_per_doc),
        "min": min(num_words_per_doc),
        "max": max(num_words_per_doc),
    }

    print(chalk.green("--- Stats processing: #sens/doc ---"))
    num_sens_per_doc_statistics = {
        "mean": statistics.mean(num_sens_per_doc),
        "std": statistics.stdev(num_sens_per_doc),
        "min": min(num_sens_per_doc),
        "max": max(num_sens_per_doc),
    }

    print(chalk.green("--- Stats processing: #words/sen ---"))
    num_words_per_sen_statistics = {
        "mean": statistics.mean(num_words_per_sen),
        "std": statistics.stdev(num_words_per_sen),
        "min": min(num_words_per_sen),
        "max": max(num_words_per_sen),
    }

    output_path = (
        output
        if output
        else os.path.splitext(os.path.basename(input))[0] + "_statistics" + ".xlsx"
    )
    output_path = data_path / output_path

    print(chalk.green("--- Writing stats to file ---"))
    out_df = pd.DataFrame(
        {
            "Number of words per sentence": num_words_per_sen_statistics,
            "Number of sentences per doc": num_sens_per_doc_statistics,
            "Number of words per doc": num_words_per_doc_statistics,
        }
    )

    with pd.ExcelWriter(
        output_path, engine="xlsxwriter", options={"strings_to_urls": False}
    ) as writer:
        out_df.to_excel(writer)

    print(chalk.blue("- Number of words per sentence:"))
    print(chalk.yellow("   + Mean:     %f" % num_words_per_sen_statistics["mean"]))
    print(chalk.yellow("   + S.Deviation: %f" % num_words_per_sen_statistics["std"]))
    print(chalk.yellow("   + Min:      %f" % num_words_per_sen_statistics["min"]))
    print(chalk.yellow("   + Max:      %f" % num_words_per_sen_statistics["max"]))

    print("- Number of sentences per doc:")
    print(chalk.yellow("   + Mean:     %f" % num_sens_per_doc_statistics["mean"]))
    print(chalk.yellow("   + S.Deviation: %f" % num_sens_per_doc_statistics["std"]))
    print(chalk.yellow("   + Min:      %f" % num_sens_per_doc_statistics["min"]))
    print(chalk.yellow("   + Max:      %f" % num_sens_per_doc_statistics["max"]))

    print("- Number of words per doc:")
    print(chalk.yellow("   + Mean:     %f" % num_words_per_doc_statistics["mean"]))
    print(chalk.yellow("   + S.Deviation: %f" % num_words_per_doc_statistics["std"]))
    print(chalk.yellow("   + Min:      %f" % num_words_per_doc_statistics["min"]))
    print(chalk.yellow("   + Max:      %f" % num_words_per_doc_statistics["max"]))


def number_of_words(text, count_all):
    words = text.split(" ")
    if not count_all:
        words = [w for w in words if contain_alphabet(w)]
    else:
        words = [w for w in words]
    return len(words)


def contain_alphabet(text):
    return re.search("\w", text) != None


if __name__ == "__main__":
    main()
